// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  region: 'ap-south-1',
  userPoolId: 'ap-south-1_2H0RRQkQR',
  userPoolWebClientId: '2qtg62l6qdfpg8eneuvn5b8k9p',
  baseUrl: 'https://dev-api.solvezy.net/pauth/bil/campaign',
  // baseUrl: 'http://172.16.3.152:8080/bil/campaign'
  // baseUrl: 'http://devnlb-api.solvezy.net/bil/campaign',
  code: ''
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
