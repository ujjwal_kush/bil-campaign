import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private http: HttpClient) { }

  saveLead(body: any) {
    const url = environment.baseUrl + '/saveLead';
    return this.http.post(url, body);
  }

  verifyPan(panNo: any) {
    const url = environment.baseUrl + '/verifyPan';
    return this.http.post(url, panNo);
  }

  sendConsumerBureauOTP(paylaod: any) {
    const url = environment.baseUrl + '/generateCBOtp';
    return this.http.post(url, paylaod);
  }

  checkConsumerBureauOTP(otp: any) {
    const url = environment.baseUrl + '/validateCBOtp';
    return this.http.post(url, otp);
  }

  checkEligibility(body: any) {
    const url = environment.baseUrl + '/checkEligibility';
    return this.http.post(url, body);
  }

  checkSolvUser(mobileNumber: string) {
    const mobileNo = encodeURIComponent(mobileNumber);
    const url = environment.baseUrl + '/checkSolvUser?mobileNumber=' + mobileNo;
    return this.http.get(url).toPromise();
  }

  getCoginitoIdentityData() {
    const applicationData = localStorage;
    const bilAuthData = {};
    const BilAuthServiceDataKey = Object.keys(applicationData);

    BilAuthServiceDataKey.forEach((key) => {
      if (key.includes('CognitoIdentityServiceProvider')) {
        const AuthArray = key.split('.');
        const AuthObjKey = AuthArray[AuthArray.length - 1];
        bilAuthData[AuthObjKey] = applicationData[key];
      }
    });
    return bilAuthData;
  }

  getAuthToken() {
    const data = this.getCoginitoIdentityData();
    // tslint:disable-next-line: no-string-literal
    return data['accessToken'] || null;
  }
}
