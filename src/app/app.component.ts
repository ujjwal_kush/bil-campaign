import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from "../environments/environment";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'solvLoanLP';
  
  public isCampaignURL: boolean = false;
  public url_parameters: Object;

  constructor(
    private route: ActivatedRoute
  ) {
    this.route.queryParams.subscribe(params => {
      this.url_parameters = params;
      if (this.url_parameters["utm_source"] && environment.production) {
        localStorage.setItem("campaign", this.url_parameters["utm_source"]);
        this.isCampaignURL = true;
      } 

      if (environment.production && this.isCampaignURL) {
        const script1 = document.createElement("script");
        script1.async = true;
        script1.src =
          "https://www.googletagmanager.com/gtag/js?id=" + environment.code;

        var script = document.createElement("script");
        script.type = "text/javascript";
        script.innerHTML = `window.dataLayer = window.dataLayer || [];
                       function gtag() { dataLayer.push(arguments); }
                       gtag('js', new Date());
                       gtag('config', 'UA-143351251-4');`;
        document.head.prepend(script);
      }
    });
  }
}
