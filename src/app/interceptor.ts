import { Injectable, NgZone } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { CommonService } from '../app/services/common.service';

@Injectable()
export class Interceptor implements HttpInterceptor {
    constructor(private commonService: CommonService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const timeStamp = Date.now().toString();
        const authToken = this.commonService.getAuthToken();
        let headers: any;
        if (request.url.endsWith('/saveLead') || request.url.includes('/checkSolvUser')) {
            headers = new HttpHeaders({
                'X-REQUEST-ID': timeStamp,
                Authorization: 'Bearer ' + authToken
            });
        } else {
            headers = new HttpHeaders({
                'X-REF-ID': localStorage.getItem('campaignId'),
                'X-REQUEST-ID': timeStamp,
                Authorization: 'Bearer ' + authToken,
                mock: 'false'
            });
        }

        request = request.clone({ headers });

        return next.handle(request).pipe(
            tap((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) { }
            }),
            catchError(err => {
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 403 || err.status === 401) { } else { }
                }
                return throwError(err);
            })
        );
    }
}
