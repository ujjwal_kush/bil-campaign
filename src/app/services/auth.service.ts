import { Injectable, Inject } from '@angular/core';
import { Auth } from 'aws-amplify';
import { CognitoUser } from 'amazon-cognito-identity-js';

import * as  crypto from 'crypto-js';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private window: Window;

  private cognitoUser: CognitoUser & { challengeParam: { mobile: string } };
  constructor(@Inject(DOCUMENT) private document: Document) {
    this.window = this.document.defaultView;
  }

  private getRandomString(bytes: number) {
    const randomValues = new Uint8Array(bytes);
    this.window.crypto.getRandomValues(randomValues);
    return Array.from(randomValues).map(this.intToHex).join('');
  }

  private intToHex(nr: number) {
    return nr.toString(16).padStart(2, '0');
  }

  // tslint:disable-next-line: variable-name
  public async signUp(fullName: string, mobile: string, email: string, preferred_username: string) {
    const params = {
      username: mobile,
      password: this.getRandomString(30),
      email,
      attributes: {
        name: fullName,
        phone_number: mobile,
        email,
        preferred_username
      }
    };
    await Auth.signUp(params);
  }

  public async signIn(mobile: string) {
    this.cognitoUser = await Auth.signIn(mobile);
  }

  public async answerCustomChallenge(data: string) {
    this.cognitoUser = await Auth.sendCustomChallengeAnswer(this.cognitoUser, data);
    return this.isAuthenticated();
  }


  public async isAuthenticated() {
    try {
      await Auth.currentSession();
      return true;
    } catch {
      return false;
    }
  }


  public async signOut() {
    await Auth.signOut();
    localStorage.clear();
  }


}
