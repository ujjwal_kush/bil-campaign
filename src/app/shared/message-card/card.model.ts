export class CardMessage {
    icon: string;
    cardInfoMsg: string;
    heading: string;
    text: string;
    bottomText: string;
}
