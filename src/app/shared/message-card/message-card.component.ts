import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { CardMessage } from './card.model';

@Component({
  selector: 'app-message-card',
  templateUrl: './message-card.component.html',
  styleUrls: ['./message-card.component.scss']
})
export class MessageCardComponent implements OnInit, OnChanges {
  @Input() message: string;
  cardMessage: CardMessage = {
    icon: '',
    cardInfoMsg: '',
    heading: '',
    text: '',
    bottomText: ''
  };

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes.message.currentValue);
    if (changes.message) {
      switch (changes.message.currentValue) {
        case 'alreadyRegistered':
          console.log('already');
          console.log(this.cardMessage);
          this.cardMessage.icon = 'assets/img/file-found.svg';
          this.cardMessage.cardInfoMsg = '';
          this.cardMessage.heading = 'You are already registered with SOLV.';
          this.cardMessage.text = 'One of our representatives will contact you shortly.';
          this.cardMessage.bottomText = '';
          console.log(this.cardMessage);
          break;
        case 'invalidCBOTP':
          this.cardMessage.icon = 'assets/img/failed-circle.svg';
          this.cardMessage.cardInfoMsg = '';
          this.cardMessage.heading = 'Invalid OTP has been entered';
          this.cardMessage.text = 'Please try again from the beginning';
          this.cardMessage.bottomText = '';
          break;
        case 'eligibleForLoan':
          this.cardMessage.icon = 'assets/img/congrats.svg';
          this.cardMessage.cardInfoMsg = 'Congratulations';
          this.cardMessage.heading = 'You are eligible for a business loan.';
          this.cardMessage.text = ' We will contact you with more details shortly.';
          this.cardMessage.bottomText = 'A FREE copy of your Credit Report has been emailed to you.';
          break;
        case 'notEligibleForLoan':
          this.cardMessage.icon = 'assets/img/failed-circle.svg';
          this.cardMessage.cardInfoMsg = '';
          this.cardMessage.heading = 'Thank you for your interest.';
          this.cardMessage.text = 'Unfortunately, your application does not meet our eligibility criteria.';
          this.cardMessage.bottomText = 'A FREE copy of your Credit Report has been emailed to you.';
          break;
        case 'notAbleFound':
          this.cardMessage.icon = 'assets/img/file-not-found.svg';
          this.cardMessage.cardInfoMsg = '';
          this.cardMessage.heading = 'We were unable to fetch your credit report.';
          this.cardMessage.text = 'One of our representatives will contact you shortly.';
          this.cardMessage.bottomText = '';
          break;
      }
    }
  }

}
