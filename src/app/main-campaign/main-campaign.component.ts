import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-main-campaign',
  templateUrl: './main-campaign.component.html',
  styleUrls: ['./main-campaign.component.scss'],
})
export class MainCampaignComponent implements OnInit {

  fullName: string;
  businessName: string;
  countryCode = 91;
  mobileNumber: string;
  otpNumber = '';
  otpTimer: number;

  panNumber: string;
  emailAddress: string;
  countryCodePan = 91;
  mobileNumberAsPAN: string;
  stgOneHitId: string;
  stgTwoHitId: string;

  campaignName = 'Feb2020';

  panVerified = false;
  inValidPAN = false;
  panChecked = false;

  primaryRegistration = true;
  secondaryRegistration = false;

  otpSent = false;
  otpVerified = false;
  correctOTP = false;

  loanEligible = false;
  loanNotEligible = false;
  loanNotExist = false;

  errorMsgCard = false;
  cardMessage: string;

  // cardMessage = 'alreadyRegistered';

  // errorMsgCard = true;
  // cardMessage = 'invalidCBOTP';

  constructor(private auth: AuthService, private commonService: CommonService) { }

  ngOnInit() {
    localStorage.setItem('campaignId', '');
  }

  public async sendOTP() {
    this.auth.signUp(this.fullName, '+91' + this.mobileNumber, this.emailAddress, this.businessName).then((response: any) => {
      console.log(response);
      this.login();
    }, (reason: any) => {
      console.log(reason);
      if (reason.code === 'UsernameExistsException') {
        this.login();
      }
    });
  }


  public login() {
    this.auth.signIn('+91' + this.mobileNumber).then((reason: any) => {
      this.timer();
      this.otpSent = true;
    }, (reason: any) => {
      this.otpSent = true;
    });
  }

  timer() {
    console.log('timer running');
    this.otpTimer = 120;
    const timer = setInterval(() => {
      this.otpTimer--;
      if (this.otpTimer === 0) {
        clearInterval(timer);
      }
    }, 1000);
  }

  onOTPKey(e, otpType: string) {
    console.log(e.target.value);
    if (otpType === 'singUpOtp') {
      if (e.target.value.length === 4) {
        this.verifyOTP(e.target.value);
      }
    } else if (otpType === 'CBOtp') {
      if (e.target.value.length === 6) {
        this.verifyCBOtp(e.target.value);
      }
    }
  }

  async verifyOTP(otp: string) {
    try {
      const data = otp;
      const loginSucceeded = await this.auth.answerCustomChallenge(data);
      console.log(loginSucceeded);
      if (loginSucceeded) {
        console.log(loginSucceeded);
        this.otpVerified = true;
        this.correctOTP = true;
      } else {
        console.log('invalid OTP');
        this.otpVerified = true;
        this.correctOTP = false;
      }
    } catch (err) {
      this.otpVerified = true;
      this.correctOTP = false;
    } finally { }
  }



  sendCBOtp() {
    const payload = {
      pan: this.panNumber.toUpperCase(),
      email: this.emailAddress,
      mobileNo: this.mobileNumberAsPAN
    };
    this.commonService.sendConsumerBureauOTP(payload).subscribe((response: any) => {
      this.stgOneHitId = response.stgOneHitId;
      this.stgTwoHitId = response.stgTwoHitId;
      this.otpSent = true;
      this.timer();
    }, (reason: any) => {
      console.log(reason);
    });
  }

  verifyCBOtp(otp: string) {
    const payload = {
      pan: this.panNumber.toUpperCase(),
      mobileNo: this.mobileNumberAsPAN,
      otp,
      stgOneHitId: this.stgOneHitId,
      stgTwoHitId: this.stgTwoHitId
    };
    this.commonService.checkConsumerBureauOTP(payload).subscribe((response: any) => {
      console.log(response);
      this.errorMsgCard = false;
    }, (reason: any) => {
      console.log(reason);
      if (reason.status === 400 && reason.error.errorMessage.includes('consumer record not found')) {
        console.log('consumer record not found');
      } else if (reason.status === 400 && reason.error.errorMessage.includes('wrong otp')) {
        this.errorMsgCard = true;
        this.cardMessage = 'invalidCBOTP';
      }
    });
  }

  public async proceed() {
    this.commonService.checkSolvUser(this.mobileNumber).then((response: any) => {
      console.log(response);
      if (response.data.existingUser) {
        this.primaryRegistration = false;
        this.secondaryRegistration = false;
        this.errorMsgCard = true;
        this.cardMessage = 'alreadyRegistered';
      } else {
        const payload = {
          campaignName: this.campaignName,
          companyName: this.businessName,
          email: this.emailAddress,
          mobileNo: this.mobileNumber,
          name: this.fullName
        };
        this.commonService.saveLead(payload).subscribe(
          (data: any) => {
            console.log(data);
            localStorage.setItem('campaignId', data.data.ref_id);
            this.primaryRegistration = false;
            this.secondaryRegistration = true;
            this.otpSent = false;
            this.otpVerified = false;
            this.correctOTP = false;
            this.otpNumber = '';
          },
          (reason: any) => {
            console.log(reason);
          });
      }
    }, (error: any) => {
      console.log(error);
    });
  }

  public async checkPAN() {
    const payload = {
      pan: this.panNumber.toUpperCase()
    };
    this.commonService.verifyPan(payload).subscribe((response: any) => {
      this.panVerified = response.data.valid;
    }, (reason: any) => {
      if (reason.error.errorMessage) {
        if (reason.error.errorMessage.includes('provide valid Pan')) {
          this.panChecked = true;
          this.inValidPAN = true;
        } else if (reason.error.errorMessage.includes('Entered pan is not a sole propritor PAN')) {
          this.panChecked = true;
          this.inValidPAN = false;
          // this.secondaryRegistration = false;
        }
      }
    });
  }

  checkEligibility() {
    const payload = {
      email: this.emailAddress,
      mobileNo: this.mobileNumberAsPAN,
      pan: this.panNumber.toUpperCase()
    };
    this.commonService.checkEligibility(payload).subscribe((response: any) => {
      console.log(response);
      this.secondaryRegistration = false;
      if (response.data && response.data.statusCode === 101) {
        this.errorMsgCard = true;
        this.cardMessage = 'notEligibleForLoan';
      } else if (response.data && response.data.statusCode === 102) {
        this.errorMsgCard = true;
        this.cardMessage = 'notAbleFound';
      } else if (response.data && response.data.statusCode === 101) {
        this.errorMsgCard = true;
        this.cardMessage = 'eligibleForLoan';
      }
    }, (reason: any) => {
      console.log(reason);
      // this.errorMsgCard = true;
      // this.cardMessage = 'serverError';
    });
  }

}
