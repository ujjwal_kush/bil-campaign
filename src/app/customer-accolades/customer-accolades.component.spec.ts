import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerAccoladesComponent } from './customer-accolades.component';

describe('CustomerAccoladesComponent', () => {
  let component: CustomerAccoladesComponent;
  let fixture: ComponentFixture<CustomerAccoladesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerAccoladesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerAccoladesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
