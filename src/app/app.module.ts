import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainCampaignComponent } from './main-campaign/main-campaign.component';

import { Interceptor } from './interceptor';
import { BenefitsComponent } from './benefits/benefits.component';
import { CustomerAccoladesComponent } from './customer-accolades/customer-accolades.component';
import { FooterComponent } from './footer/footer.component';
import { MessageCardComponent } from './shared/message-card/message-card.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainCampaignComponent,
    BenefitsComponent,
    CustomerAccoladesComponent,
    FooterComponent,
    MessageCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: Interceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
